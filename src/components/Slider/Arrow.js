import React from 'react'
import styled from 'styled-components'

const Arrow =styled.div `
  
  
    {
      display: flex;
      position: absolute;
      top: 0;
     ${props => props.direction=== 'right' ? `right: 25px` : `left: 25px`};
      height: 100%;
      width: 50px;
      justify-content: center;
      background: rgba(255,255,255,.6);
  
      cursor: pointer;
      align-items: center;
      transition: transform ease-in 0.1s;
      &:hover {
        transform: scale(1.4);
      }
      i{
          font-size:30px
      }
      }
    }
    `
 


export default Arrow