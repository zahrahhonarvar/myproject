import styled from 'styled-components';
export const SliderContent = styled.div`
   {
    height: 100%;
    transform: translateX(${(props) => props.translate}px);
    transition: transform ease-out ${(props) => props.transition}s;
    width: 100%;
    display: flex;
  }
`;

export const Image = styled.img`
   {
    height: 100%;
    width: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
  }
`;
export const Img = styled.img`
   {
    width: 100px;
    padding: 10px;
    margin-right: 5px;
    cursor: pointer;
    background-color: #ebebeb;
    border: 3px solid ${(props) => (props.isActive ? '#ed1c24' : 'white')};
  }
`;
