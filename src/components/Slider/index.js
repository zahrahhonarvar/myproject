import React, { useState } from 'react';
import { Image, List, SliderContent, Img } from './style';
// import "../../App.css";

import Arrow from './Arrow';
export default function Slider({ items, imageSize }) {
  const getWidth = () => window.innerWidth;
  console.log(getWidth());
  const [state, setState] = useState({
    activeIndex: 0,
    translate: 0,
    transition: 0.45,
  });

  const { activeIndex, translate, transition } = state;

  const prevSlide = () => {
    if (activeIndex === 0) {
      return setState({
        ...state,
        translate: (items.length - 1) * -imageSize,
        activeIndex: items.length - 1,
      });
      console.log('ok');
    }

    setState({
      ...state,
      activeIndex: activeIndex - 1,
      translate: (activeIndex - 1) * -imageSize,
    });
    console.log('ok');
  };

  const nextSlide = () => {
    if (activeIndex === items.length - 1) {
      return setState({
        ...state,
        translate: 0,
        activeIndex: 0,
      });
    }

    setState({
      ...state,
      activeIndex: activeIndex + 1,
      translate: (activeIndex + 1) * -imageSize,
    });
  };
  function goToSlide(index) {
    setState({
      ...state,
      activeIndex: index,
      translate: activeIndex * -imageSize,
    });
  }
  return (
    <>
      {' '}
      <div
        className="parent"
        style={{ height: '100vh', width: 'auto', margin: '0 auto', overflow: 'hidden', position: 'relative' }}
      >
        {' '}
        <SliderContent translate={translate} transition={transition}>
          {items.map((item) => {
            return <Image key={item.id} src={item.src} />;
          })}
        </SliderContent>
        <Arrow onClick={prevSlide} direction="left">
          <i className="fa fa-angle-left" />
        </Arrow>
        <Arrow onClick={nextSlide} direction="right">
          <i className="fa fa-angle-right" />
        </Arrow>
        <div
          className="dot"
          style={{
            listStyle: 'none',
            position: 'absolute',
            bottom: '0',
            zIndex: '1000',
          }}
        >
          {items.map((item, index) => {
            return (
              <Img
                src={item.src}
                key={index}
                index={index}
                activeIndex={activeIndex}
                isActive={activeIndex == index}
                onClick={() => goToSlide(index)}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}
