import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const AccordionWrapper = styled.div`
  border-top: 1px solid ${Variables.colors.lightGray};
`;
export const AccordionRow = styled.div``;
export const Title = styled.h1`
  margin: 0;
  padding: 10px 0;
  text-transform: uppercase;
  color: ${Variables.colors.blackLight};
`;
export const Icon = styled.div`
  margin: 0;
  padding: 10px 0;
  i.accordion-icon {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 32px;
    height: 32px;
    transition-property: transform;
    transition-duration: 0.2s;
    transition-timing-function: linear;
    ${(props) => {
      if (props.show) {
        return `transform: rotate(180deg)`;
      } else {
        return `transform: rotate(360deg)`;
      }
    }};
    &:before {
      color: ${Variables.colors.blackLight};
    }
  }
`;
export const Header = styled.div`
  cursor: pointer;
  width: 100%;
  border-bottom: 1px solid ${Variables.colors.blackLight};
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const Body = styled.div`
  min-height: 50px;
  width: 100%;
  margin-top: 27px;
  .accordion-ul {
    margin: 0;
    padding: 0;
    display: inline-block;
    .accordion-li {
      display: flex;
      justify-content: center;
      align-items: center;
      position: relative;
      padding-left: 16px;
      margin-bottom: 27px;
      &:before {
        content: ' ';
        width: 6px;
        height: 6px;
        border-radius: 50%;
        background-color: ${Variables.colors.black};
        position: absolute;
        left: 0;
        top: calc(50% - 4px);
      }
    }
  }
`;
export const Key = styled.div`
  font-weight: 600;
  font-size: 16px;
  color: ${Variables.colors.black};
`;
export const Value = styled.div`
  margin-left: 8px;
  font-size: 14px;
  color: ${Variables.colors.blackLight};
`;
