import React, { useState } from 'react';
import { AccordionWrapper, AccordionRow, Header, Body, Title, Icon, Key, Value } from './style';

export default ({ items }) => {
  const [status, setStatus] = useState(false);
  const [id, setId] = useState(null);
  const toggleMenu = (key) => {
    if (Number(key) === Number(id)) {
      setStatus(!status);
    } else {
      setStatus(true);
    }
    setId(key);
  };
  return (
    <AccordionWrapper>
      {items.map((item, key) => {
        return (
          <AccordionRow key={key}>
            <Header onClick={() => toggleMenu(key)}>
              <Title>{item.title}</Title>
              <Icon show={status && id === key}>
                <i className="accordion-icon icon-down-open-big" />
              </Icon>
            </Header>
            {status && id === key && (
              <Body>
                <ul className="accordion-ul">
                  {item.description.map((itm, key) => {
                    return (
                      <li key={key} className="accordion-li">
                        <Key>{itm.key}: </Key>
                        <Value>{itm.value}</Value>
                      </li>
                    );
                  })}
                </ul>
              </Body>
            )}
          </AccordionRow>
        );
      })}
    </AccordionWrapper>
  );
};
