import React from 'react';
import { RateWrapper } from './style';

export default ({ rate }) => {
  const makeStars = (rate) => {
    const _rate = rate && rate <= 5 && rate >= 1 ? rate : 0;
    let stars = [];
    for (let i = 1; i <= 5; i++) {
      if (i <= _rate) {
        stars.push(<i key={Math.random()} className="full icon-star" />);
      } else {
        stars.push(<i key={Math.random()} className="empty icon-star-empty" />);
      }
    }
    return stars;
  };
  return <RateWrapper>{makeStars(rate)}</RateWrapper>;
};
