import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const RateWrapper = styled.div`
	padding-top: 48px;
	width: 100%;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-wrap: nowrap;
	i.full {
		&:before {
			color: ${Variables.colors.gold};
		}
	}
`;