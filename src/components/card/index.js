import React from 'react';
import { CardWrapper } from './style';


export default () => {
	return (
		<CardWrapper>
			<i className="card-icon icon-basket"/>
		</CardWrapper>
	);
}