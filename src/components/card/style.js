import styled from 'styled-components';
import Variables from '../../constants/styleVariables';


export const CardWrapper = styled.div`
	cursor: pointer;
	height: 80px;
	min-width: 80px;
	background-color: ${Variables.colors.whiteGray};
	display: flex;
	align-items: center;
	justify-content: center;
	margin-left: 21px;
	.card-icon {
		display: flex;
		align-items: center;
		justify-content: center;
		&:before {
			font-size: 24px;
		}
	}
`;