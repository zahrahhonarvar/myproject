import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const CarouselWrapper = styled.div`
  min-height: 700px;
  .carousel-img {
    min-height: 700px;
    min-width: 700px;
  }
  margin-bottom: 27px;
`;

export const Thumbnails = styled.div`
  width: 100%;
  .thumbnails-list {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin: 0;
    padding: 0;
    .thumbnails-list-items {
      list-style: none;
      width: 88px;
      height: 110px;
      background-color: ${Variables.colors.whiteGray};
      margin-left: 20px;
      .thumbnails-list-items-img {
        width: 88px;
        height: 110px;
      }
    }
  }
`;
