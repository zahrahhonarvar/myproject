import React from 'react';
import { CarouselWrapper, Thumbnails } from './style';

export default () => {
  return (
    <CarouselWrapper>
      <img className="carousel-img" src="images/products/5150_profile.png" alt="product" />
      <Thumbnails>
        <ul className="thumbnails-list">
          <li className="thumbnails-list-items">
            <img className="thumbnails-list-items-img" src="images/products/5150_profile.png" alt="thumbnails" />
          </li>
          <li className="thumbnails-list-items">
            <img className="thumbnails-list-items-img" src="images/products/5150_profile.png" alt="thumbnails" />
          </li>
          <li className="thumbnails-list-items">
            <img className="thumbnails-list-items-img" src="images/products/5150_profile.png" alt="thumbnails" />
          </li>
        </ul>
      </Thumbnails>
    </CarouselWrapper>
  );
};
