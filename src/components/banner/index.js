import React from 'react';
import { BannerWrapper, Banner } from './style';

export default () => {
	return (
		<BannerWrapper>
			<Banner>
				<div className="text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					<span>
						aspernatur consequuntur corporis cumque.
					</span>
				</div>
			</Banner>
		</BannerWrapper>
	);
};