import styled from 'styled-components';
import Variables from '../../constants/styleVariables';


export const BannerWrapper = styled.div`
	background-color: ${Variables.colors.blackLight};
	width: 100%;
	padding: 10px 20px 8px;
`;

export const Banner = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100%;
	max-width: 1600px;
	margin: 0 auto;
	.text {
		letter-spacing: 1px;
		font-family: 'Barlow',Arial,sans-serif;
		font-size: 16px;
		text-transform: uppercase;
		color: ${Variables.colors.white};
		span {
			color: ${Variables.colors.red};
			font-weight: 600;
		}
	}
`;