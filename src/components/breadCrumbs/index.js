import React from 'react';
import { BreadCrumbsWrapper, BreadCrumbs } from './style';


export default () => {
	return (
		<BreadCrumbsWrapper>
			<BreadCrumbs>
				<ul className='bread-crumbs-list'>
					<li>
						<i className='icon-left-open-big'/>
					</li>
					<li className='bread-crumbs-list-item'>
						<span>home</span>
					</li>
					<li className='bread-crumbs-list-item bread-crumbs-list-item-active'>
						<span>/</span>
					</li>
					<li className='bread-crumbs-list-item bread-crumbs-list-item-active'>
						<span>lucha</span>
					</li>
				</ul>
			</BreadCrumbs>
		</BreadCrumbsWrapper>
	);
}