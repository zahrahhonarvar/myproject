import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const BreadCrumbsWrapper = styled.div`
	width: 100%;
`;
export const BreadCrumbs = styled.div`
	width: 100%;
	max-width: 1600px;
	padding: 17px 0;
	margin-bottom: 17px;
	background-color: ${Variables.colors.whiteGray};
	.bread-crumbs-list {
		width: 1240px;
		margin: 0 auto;
		padding: 0 20px;
		display: flex;
		justify-content: flex-start;
		align-items: center;
		list-style: none;
		.bread-crumbs-list-item {
			margin-left: 10px;
			span {
				text-transform: uppercase;
			}
		}
		.bread-crumbs-list-item-active {
			color: ${Variables.colors.lighterGray};
		}
	}
`;