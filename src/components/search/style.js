import styled from 'styled-components';
import Variables from '../../constants/styleVariables';


export const SearchBox = styled.div`
	border: 1px solid ${Variables.colors.lightGray};
	height: 40px;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	.search-form {
		width: 85%;
	}
	.search-box-icon {
		width: 15%;
		&:before {
			font-size: 24px;
			color: ${Variables.colors.blackLight};
		}
	}
	.search-input {
		width: 100%;
		height: 100%;
		padding: 8px;
		border: unset;
		font-size: 14px;
	}
`;