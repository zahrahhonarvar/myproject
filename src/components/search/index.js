import React, {useState} from 'react';
import { SearchBox } from "./style";


export default () => {
	const [value, setValue] = useState('');
	const handleSubmit = (e) => {
		e.preventDefault();
		// handle form submit ...
	}
	const handleChange = (e) => {
		e.preventDefault();
		setValue(e.target.value);
	}
	return (
		<SearchBox>
			<form className='search-form' onSubmit={handleSubmit}>
				<input
					className="search-input"
					name='search'
					type="text"
					onChange={handleChange}
					value={value}
					placeholder={'search'}
				/>
			</form>
			<i className='search-box-icon icon-search'/>
		</SearchBox>
	);
}