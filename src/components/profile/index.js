import React from 'react';
import { ProfileWrapper } from './style';


export default () => {
	return (
		<ProfileWrapper>
			<i className='profile-icon icon-torso'/>
		</ProfileWrapper>
	);
}