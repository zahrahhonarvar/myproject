import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const ProfileWrapper = styled.div`
	cursor: pointer;
	height: 80px;
	background-color: ${Variables.colors.white};
	display: flex;
	align-items: center;
	justify-content: center;
	margin-left: 21px;
	.profile-icon {
		&:before {
			font-size: 24px;
		}
	}
`;
