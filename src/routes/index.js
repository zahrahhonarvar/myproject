import React, { lazy } from "react";
const ProjectsManagement = lazy(() => import ("../containers/projectsManagement"));

const routes = [
	{
		path: "/projects-management",
		component: ProjectsManagement,
		layout: "fullpage",
		visibility: "trust"
	},

];
export default routes;
