import styled, { createGlobalStyle } from 'styled-components';


export const GlobalStyle = createGlobalStyle`
	body {
		padding: 0;
		margin: 0;
	}
	* {
		box-sizing: border-box;
		outline: 0;
		font-family: 'Montserrat',Arial,sans-serif;
	}
`;