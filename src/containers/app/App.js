import React from 'react';
import { GlobalStyle } from './style';
import MainLayout from '../../layouts/main';

function App() {
  return (
    <>
      <GlobalStyle/>
      <MainLayout>
        <div>hello world</div>
      </MainLayout>
    </>
  );
}

export default App;
