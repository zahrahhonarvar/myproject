import React from 'react';
import Rate from '../../../../components/rate';
import { Cost, Model, ProductDescription, ProductLinks, ProductTitle, Review, SocialNetworks } from './style';

export default () => {
  return (
    <>
      <Rate rate={4} />
      <Review>4 reviews</Review>
      <ProductTitle>lucha</ProductTitle>
      <Model>model 5150</Model>
      <Cost>$189.99</Cost>
      <ProductDescription>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci</ProductDescription>
      <ProductLinks>
        <div className="compare">
          <i className="links-icon icon-heart" />
          ADD TO MY WISH LIST
        </div>
        <div className="wish-list">
          <img
            className="links-icon"
            src="https://kershaw.kaiusa.com/static/version1593607237/frontend/BlueAcorn/kershaw/en_US/images/compare-icon.png"
            alt="compare"
          />
          compare
        </div>
      </ProductLinks>
      <SocialNetworks>
        <div className="social-networks-text">share: </div>
        <ul className="social-networks-icons">
          <li className="social-networks-items">
            <i className="icon-pinterest" />
          </li>
          <li className="social-networks-items">
            <i className="icon-twitter" />
          </li>
          <li className="social-networks-items">
            <i className="icon-facebook" />
          </li>
        </ul>
        <div className="more">
          <i className="icon-plus" />
        </div>
      </SocialNetworks>
    </>
  );
};
