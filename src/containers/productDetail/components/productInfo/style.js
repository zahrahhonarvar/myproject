import styled from 'styled-components';
import Variables from '../../../../constants/styleVariables';

export const Review = styled.div`
  text-transform: uppercase;
  font-size: 16px;
  color: ${Variables.colors.black};
  margin-top: 3px;
  margin-bottom: 16px;
`;

export const ProductTitle = styled.div`
  font-weight: 600;
  text-transform: uppercase;
  color: ${Variables.colors.black};
  font-size: 32px;
  margin-top: 4px;
`;

export const Model = styled.div`
  text-transform: uppercase;
  color: ${Variables.colors.black};
  font-size: 16px;
  margin-top: 4px;
`;

export const Cost = styled.div`
  color: ${Variables.colors.darkRed};
  font-size: 24px;
  font-weight: 500;
  margin-top: 4px;
  font-family: 'Barlow', Arial, sans-serif;
`;

export const ProductDescription = styled.div`
  text-transform: uppercase;
  font-size: 16px;
  color: ${Variables.colors.blackLight};
  letter-spacing: 1px;
  margin-top: 16px;
`;

export const ProductLinks = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid ${Variables.colors.lightGray};
  padding-bottom: 15px;
  margin-top: 22px;
  .compare,
  .wish-list {
    color: ${Variables.colors.lighterGray};
    font-size: 16px;
    text-transform: uppercase;
    .links-icon {
      margin-right: 8px;
      width: 16px;
      height: 16px;
      &:before {
        font-size: 16px;
        color: #cac9c9;
      }
    }
  }
`;

export const SocialNetworks = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 17px 0;
  .social-networks-text {
    font-size: 16px;
    letter-spacing: 1px;
    line-height: 0.88px;
    color: #25292d;
    text-transform: uppercase;
    margin: 0;
    width: 20%;
  }
  .social-networks-icons {
    width: 60%;
    padding: 0;
    margin: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    .social-networks-items {
      width: 23%;
      list-style: none;
      i {
        &:before {
          color: ${Variables.colors.black};
          font-size: 20px;
        }
      }
    }
  }
  .more {
    width: 20%;
    i {
      &:before {
        color: ${Variables.colors.black};
        font-size: 20px;
      }
    }
  }
`;
