import styled from 'styled-components';
import Variables from '../../../../constants/styleVariables';

export const AboutWrapper = styled.div`
  border-top: 1px solid ${Variables.colors.lightGray};
`;
export const Title = styled.div`
  font-weight: 600;
  font-size: 18px;
  margin: 27px 0 22px 0;
  letter-spacing: 1px;
  text-transform: uppercase;
  width: 100%;
  text-align: left;
  color: ${Variables.colors.black};
  font-family: 'Barlow', Arial, sans-serif;
`;
export const Description = styled.p`
  text-align: left;
  font-size: 16px;
  color: ${Variables.colors.black};
  line-height: 22.85px;
  margin-bottom: 10px;
  font-family: 'Barlow', Arial, sans-serif;
`;
export const List = styled.ul`
  margin: 0 0 0 16px;
  padding: 0;
  li {
    font-family: 'Barlow', Arial, sans-serif;
    margin-bottom: 11px;
    font-size: 16px;
  }
`;
