import React from 'react';
import { AboutWrapper, Title, Description, List } from './style';

export default () => {
  return (
    <AboutWrapper>
      <Title>About</Title>
      <Description>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut dolorem eligendi est fugiat in ipsa
        iste minima necessitatibus nihil nostrum, perferendis quae quasi ratione repudiandae vel velit voluptas
        voluptatibus!
      </Description>
      <List>
        <li>Lorem ipsum dolor sit amet, consectetur adipisicing</li>
        <li>elit. A autem corporis dolorum molestias, nihil non pariatur</li>
        <li>perferendis voluptatibus. Cupiditate distinctio error,</li>
        <li>est id in nihil perspiciatis possimus repudiandae sequi</li>
        vitae.
      </List>
    </AboutWrapper>
  );
};
