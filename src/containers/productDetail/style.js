import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const ProductDetailWrapper = styled.section`
  max-width: 1240px;
  margin: 0 auto;
  padding: 0 20px;
`;

export const ProductRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: nowrap;
`;

export const ProductColumn = styled.div`
  width: ${(props) => `${props.width}%`};
  min-height: 400px;
`;
