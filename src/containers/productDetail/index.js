import React from 'react';
import { ProductDetailWrapper, ProductRow, ProductColumn } from './style';
import Carousel from '../../components/carousel';
import image1 from '../../components/Slider/5150_clipside-closed.png';
import image2 from '../../components/Slider/5150_profile.png';
import image3 from '../../components/Slider/7008cu_profile.png';
import ProductInfo from './components/productInfo';
import About from './components/about';
import Accordion from '../../components/accordion';
import Slider from '../../components/Slider/index';

export default () => {
  return (
    <ProductDetailWrapper>
      <ProductRow>
        <ProductColumn width={57}>
          <Slider
            imageSize={700}
            items={[
              {
                id: '0',
                name: 'slide-1',
                src: image1,
              },
              {
                id: '1',
                name: 'slide-2',
                src: image2,
              },
              {
                id: '2',
                name: 'slide-3',
                src: image3,
              },
            ]}
          />
        </ProductColumn>
        <ProductColumn width={40}>
          <ProductInfo />
        </ProductColumn>
      </ProductRow>
      <ProductRow>
        <ProductColumn width={57}>
          <About />
        </ProductColumn>
        <ProductColumn width={40}>
          <Accordion
            items={[
              {
                title: 'One',
                description: [
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                ],
              },
              {
                title: 'two',
                description: [
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                ],
              },
              {
                title: 'three',
                description: [
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                ],
              },
              {
                title: 'four',
                description: [
                  {
                    key: 'foo',
                    value: 'Lorem ipsum dolor sit amet, consectetur!',
                  },
                ],
              },
            ]}
          />
        </ProductColumn>
      </ProductRow>
    </ProductDetailWrapper>
  );
};
