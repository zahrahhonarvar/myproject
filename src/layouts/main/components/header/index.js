import React from 'react';
import { HeaderWrapper, Logo, IconBox } from "./style";
import Search from '../../../../components/search';
import Profile from '../../../../components/profile';
import Cart from '../../../../components/card';


export default () => {
	return (
		<HeaderWrapper>
			<div className="left-side">
				<Logo>
					<img src="/images/logo.svg" alt="kershaw-logo"/>
					<i className="logo-icon icon-down-open-big"/>
				</Logo>
				<nav className="navbar">
					<ul className="navbar-list">
						{headerItems.map((item, key) =>
							<li key={key} className="navbar-list-item">
								<div className="navbar-list-item-text">{item}</div>
								<i className="navbar-list-item-icon logo-icon icon-down-open-big"/>
							</li>
						)}
					</ul>
				</nav>
			</div>
			<div className="right-side">
				<Search/>
				<Profile/>
				<Cart/>
			</div>
		</HeaderWrapper>
	);
}

const headerItems = ['shop', 'learn', 'support', 'about'];