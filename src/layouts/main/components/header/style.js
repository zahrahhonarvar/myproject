import styled from 'styled-components';
import Variables from '../../../../constants/styleVariables';


export const HeaderWrapper = styled.header`
	width: 100%;
	height: 80px;
	background-color: ${Variables.colors.white};
	max-width: 1240px;
	margin: 0 auto;
	padding: 20px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	.left-side {
		float: left;
		display: flex;
		justify-content: center;
		align-items: center;
		.navbar {
			margin-left: 25px;
			.navbar-list {
				margin: 0;
				padding: 0;
				display: flex;
				justify-content: center;
				align-items: center;
				.navbar-list-item {
					&:hover {
						.navbar-list-item-text {
							color: ${Variables.colors.red};
						}
						.navbar-list-item-icon {
							&:before {
								color: ${Variables.colors.red};
							}
						}
					}
					cursor: pointer;
					margin-left: 27px;
					&:first-child {
						margin-left: 0;
					}
					padding: 10px;
					height: 36px;
					list-style: none;
					display: flex;
					justify-content: center;
					align-items: center;
					.navbar-list-item-text {
						color: ${Variables.colors.black};
						font-size: 14px;
						font-weight: 600;
						text-transform: uppercase;
					}
					.navbar-list-item-icon {
						margin-left: 10px;
						&:before {
							color: ${Variables.colors.black};
						}
					}
				}
			}
		}
	}
	.right-side {
		display: flex;
		justify-content: center;
		align-items: center;
	}
`;
export const Logo = styled.div`
	align-self: flex-start;
	cursor: pointer;
	display: flex;
	justify-content: center;
	align-items: center;
	.logo-icon {
		height: 32px;
		display: flex;
		justify-content: center;
		align-items: center;
		margin-left: 6px;
		&:before {
			margin-top: 5px;
		}
	}
`;
