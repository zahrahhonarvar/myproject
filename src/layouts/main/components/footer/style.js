import styled from 'styled-components';
import Variables from '../../../../constants/styleVariables';


export const FooterWrapper = styled.footer`
	background-color: ${Variables.colors.blackLight};
	width: 100%;
	min-height: 670px;
`;

export const Footer = styled.div`
	max-width: 1240px;
	width: 100%;
	margin: 0 auto;
	padding: 0 20px;
	h1 {
		color: ${Variables.colors.white};
	}
`;
