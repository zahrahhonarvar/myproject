import styled from 'styled-components';

export const MainLayoutWrapper = styled.div``;

export const MainContainer = styled.div`
  width: 100%;
  max-width: 1600px;
  margin: 0 auto;
  min-height: 100vh;
`;

export const Content = styled.section`
  width: 100%;
  padding: 0;
  margin: 0;
  min-height: 700px;
  height: 100%;
`;
