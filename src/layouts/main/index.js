import React from 'react';
import { MainLayoutWrapper, MainContainer, Content } from './style';
import Header from './components/header';
import Footer from './components/footer';
import Banner from '../../components/banner';
import BreadCrumbs from '../../components/breadCrumbs';
import ProductDetail from '../../containers/productDetail';

export default () => {
  return (
    <MainLayoutWrapper>
      <MainContainer>
        <Header />
        <Content>
          <Banner />
          <BreadCrumbs />
          <ProductDetail />
        </Content>
        <Footer />
      </MainContainer>
    </MainLayoutWrapper>
  );
};
